#utilisation image Maven
FROM maven:3.8.1-jdk-11-openj9
#definition de l'espace de travail
WORKDIR /app
#on copie tout dans app
COPY . .
#on lance commande maven package take the compiled code and package it in its distributable format, such as a JAR.
RUN mvn package
#on ouvre le port 8080 du conteneur
EXPOSE 8080
# on execute la commande d'exécution depuis le fichier compilé .jar  présent dans target
CMD ["java", "-jar", "target/ter23-0.0.1-SNAPSHOT.jar"]